<?php

class Views 
{
    function __construct($view, $data = null)
    {
        if (file_exists("./views/$view")) {
           require_once("./views/$view");
        }else{
            die("La viesta no encontrada");
        }
    }
}
