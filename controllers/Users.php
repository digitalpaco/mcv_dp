<?php

/**
 * 
 */
class Users extends Controller
{
	
	function __construct()
	{
		parent::__construct();
	}
	public function index()
	{
		$ViewUser = new Views("users/index.php");
	}

	public function login()
	{
		$ViewUser = new Views("users/login.php");
	}
	public function register()
	{
		$ViewUser = new Views("users/register.php");
	}
}