<?php

require_once 'core/Controller.php';
require_once 'core/Config.php';
require_once 'core/Views.php';

if ($_GET && isset($_GET["controller"])) {
	$defaultController = $_GET["controller"];
	if (file_exists("controllers/" . $defaultController . ".php")) {
		require_once "controllers/" . $defaultController . ".php";
	}else{
		die("Controller no encontrado");
	}
}else{
	if (file_exists("controllers/" . $defaultController . ".php")) {
		require_once "controllers/" . $defaultController . ".php";
	}else{
		die("Controller de default no encontrado");
	}
}
$digitalpaco = new $defaultController();